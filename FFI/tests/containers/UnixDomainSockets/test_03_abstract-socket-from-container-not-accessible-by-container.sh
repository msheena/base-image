#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running af_unix test in orderly"
podman run $CONTAINER_PARAMS -d --name orderly $BASE_CONTAINER_IMAGE ./tst_af-unix > /dev/null

printf "%s\n" "-- Running af_unix test in confusion"
podman run $CONTAINER_PARAMS --name confusion $BASE_CONTAINER_IMAGE ./tst_af-unix > /dev/null

