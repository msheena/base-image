# Ostree image update

ostree image should be updated against pre created update repo.  
ostree image update as per the following doc
[ostree-image-update](https://sigs.centos.org/automotive/building/updating_ostree/)  

Test assumes there is a ostree repo prepared remote  
otree-update.fmf contains default environment parameters.  

**Note:** Please set following environment variables as per repo created:  

```shell
BASE_REPO_URL=http://<REPO_URL>/repo  
ARCH=x86_64  
IMAGE_ANNOTATION: @remote-update <or extra strings between ostree to arch>
// <or extra strings between ostree to arch>
OS_PREFIX: cs  
OS_VERSION: 9  
TARGET: qemu  
IMAGE_NAME: qa  
```

## This Test Suite includes these tests

1. Set rpm-ostree remote and upgrade  
1. Reboot  
1. Verify machine boot into ostree  

## To run tests TF provisioned auto ostree based image

```shell
testing-farm request --arch aarch64
   -e S3_BUCKET_NAME=auto-toolchain-ci-gitlab-workspaces-downstream
   -e AWS_REGION=eu-west-1
   -e IMAGE_KEY=auto-osbuild-qemu-cs9-qa-ostree-aarch64-018130c6-c31c-42d4-815d-47594385bdf8
 --git-ref new-ostree-update
 --git-url https://gitlab.com/redhat/edge/tests/base-image
 --plan /ostree-update/ostree-update
 --compose auto-osbuild-qemu-cs9-qa-ostree-aarch64-018130c6-c31c-42d4-815d-47594385bdf8
 --test-type fmf
```

## Running tests with qcow and local repository

Please refer [ostree-image-update](https://sigs.centos.org/automotive/building/updating_ostree/)  
sig-docs propose  --publish-dir=\<"\local-repo-url\"\> while running with runvm script.  
